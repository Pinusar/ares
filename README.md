Kommentaarid

Rakendus on tehtud asp.net MVC raamistikus.
Dionysus ja Ares on ühendatud sama andmebaasiga. Andmebaasis on 2 tabelit: oksjonid ja pakkumised.
Dionysuse rakenduses näeb kasutaja kõiki käimasolevaid (staatusega 1) oksjoneid. 
Kasutaja saab teha pakkumisi vajutades vastavale nupule. Seejärel viiakse ta uue pakkumise sisestamise lehele. 
Kasutaja saab sisestada oma nime ja summa, ülejäänud väärtused määratakse automaatselt (nt kellaajaks võetakse praegune kellaaeg). 
Iga oksjoni kohta näeb kasutaja reaalajas oksjoni lõpuni jäänud aega. 
Kui aktiivse oksjoni aeg saab läbi, toimub lehel automaatselt „Refresh“, et eemaldada see oksjon aktiivsete nimekirjast. Seejärel kasutaja enam oksjonit ei näe. 
Lõppenud oksjoneid näeb ainult haldusrakenduse Ares kaudu.

Arese rakenduse kaudu saab oksjoneid hallata. Saab luua uue oksjoni või vaadata käimasolevate/lõppenud oksjonite pakkumisi.
Uue oksjoni loomisel saab selle staatuseks määrata:
0 – salvestatud (võimalik hiljem ühe nupulevajutusega aktiivseks muuta)
1 – aktiivne (nähtav kasutajatele rakenduses Dionysus)
2 – lõppenud
Kui sisestada uus oksjon, mille lõpptähtaeg on minevikus, aga määrata talle staatuseks aktiivne, siis oksjonite nimekirja kuvamisel muutub selle staatus automaatselt lõppenuks.
Oksjonid kuvatakse staatuse järgi: kõigepealt salvestatud (sinised), siis aktiivsed (kollased) ja siis lõppenud (punased).
Salvestatud oksjoni saab aktiivseks muuta, vajutades nupule „Alusta oksjon“.
Aktiivsed oksjonid kuvatakse sarnaselt, nagu rakenduses Dionysus, mõningate erinevustega. 
Pakkumise tegemise asemel saab näha rohkem infot. Vajutades nupule „Rohkem infot“, on muuhulgas näha kõiki sellele tootele seni tehtud pakkumisi.
Lõppenud oksjonite puhul on info vaates näha ka oksjoni võitja nimi ja tema pakutud summa. Kui oksjonile pakkumisi ei tehtud, kuvatakse selle kohta vastav teade.
Oksjonite haldamise platvormil „Ares“ on võimalik näha ka kõiki pakkumisi korraga, valides menüüribalt valiku „Pakkumised“. 
Samuti on võimalus neid muuta või kustutada, kui nt kasutajal läks tehniliste probleemide tõttu midagi valesti.

Rakenduste disaini puhul on peetud silmas, et need oleks kasutatavad ka mobiilis.


